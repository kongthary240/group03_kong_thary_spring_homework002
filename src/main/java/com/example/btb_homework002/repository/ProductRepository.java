package com.example.btb_homework002.repository;

import com.example.btb_homework002.model.entity.Customer;
import com.example.btb_homework002.model.entity.Product;
import com.example.btb_homework002.model.request.CustomerRequest;
import com.example.btb_homework002.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface ProductRepository {
    @Select("SELECT * FROM product_tb")
    List<Product> findAllProduct();

    @Select("SELECT *FROM product_tb WHERE id=#{productId}")
    Product getProductbyID(Integer productId);

    @Delete("DELETE FROM product_tb WHERE id=#{productId}")
    boolean deleteProductById(Integer productId);

    @Select("INSERT INTO product_tb (name,price) VALUES(#{request.name},#{request.price})"+
            "RETURNING id")
    Integer saveProduct(@Param("request") ProductRequest productRequest);

    @Select("""
            UPDATE product_tb
            SET name=#{pro.name},    
            price =#{pro.price}
            WHERE id = #{productId}
            RETURNING id;
            """
    )
    Integer updateproduct(@Param("pro") ProductRequest productRequest ,Integer productId);
}
