package com.example.btb_homework002.repository;

import com.example.btb_homework002.model.entity.Customer;
import com.example.btb_homework002.model.request.CustomerRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;



@Mapper
public interface CustomerRepository {
    @Select("SELECT * FROM customer_tb")
    List<Customer> findAllCustomer();
    @Select("SELECT *FROM customer_tb WHERE id=#{customerId}")
    Customer getCustomerbyID(Integer customerId);
    @Delete("DELETE FROM customer_tb WHERE id=#{customerId}")
    boolean deleteCustomerById(Integer customerId);

    @Select("INSERT INTO customer_tb (name,address,phone) VALUES(#{request.name},#{request.address},#{request.phone})"+
    "RETURNING id")
    Integer saveCustomer(@Param("request") CustomerRequest customerRequest);

    @Select("""
            UPDATE customer_tb
            SET name=#{cus.name},
            address=#{cus.address},
            phone =#{cus.phone}
            WHERE id = #{customerId}
            RETURNING id;
            """
    )
    Integer updatecustomer(@Param("cus") CustomerRequest customerRequest ,Integer customerId);

}
