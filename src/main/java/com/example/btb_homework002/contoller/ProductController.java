package com.example.btb_homework002.contoller;

import com.example.btb_homework002.model.entity.Customer;
import com.example.btb_homework002.model.entity.Product;
import com.example.btb_homework002.model.request.CustomerRequest;
import com.example.btb_homework002.model.request.ProductRequest;
import com.example.btb_homework002.model.response.CustomerResponse;
import com.example.btb_homework002.model.response.ProductResponse;
import com.example.btb_homework002.service.CustomerService;
import com.example.btb_homework002.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }
    @GetMapping("/all")
    @Operation(summary = "Get all product")
    public ResponseEntity<ProductResponse<List<Product>>>getAllProduct(){
        ProductResponse<List<Product>> response=ProductResponse.<List<Product>>builder()
                .message("Fetch data successfully")
                .message("Fetch data successfully")
              .payload(productService.getAllProduct())
               .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
              .build();
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
   @Operation(summary = "Get product by id")
    public ResponseEntity<ProductResponse<Product>> getcProductById(@PathVariable("id") Integer productId){
        ProductResponse<Product> response=null;
        if(productService.getProductById(productId)!=null){
            response=ProductResponse.<Product> builder()
                    .message("Fetch data successfully by id")
                    .payload(productService.getProductById(productId))
                   .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
           return ResponseEntity.ok(response);
        }else {
            response=ProductResponse.<Product>builder()
                    .message("Data not found")
                   .httpStatus(HttpStatus.NOT_FOUND)
                   .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
          return ResponseEntity.badRequest().body(response);
        }
    }
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete product by id")
    public ResponseEntity<ProductResponse<String>>deleteProductById(@PathVariable("id") Integer productId){
        ProductResponse<String> response=null;
        if(productService.deleteProductId(productId)==true){
            response=ProductResponse.<String>builder()
                    .message("Delete successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }
    @PostMapping
    @Operation(summary = "Save new Product")
    public ResponseEntity<ProductResponse<Product>> addnewProduct(@RequestBody ProductRequest productRequest){
        Integer storeProductId= productService.addNewProduct(productRequest);
        if(storeProductId!=null){
            ProductResponse<Product> response=ProductResponse.<Product> builder()
                    .message("Add successfully")
                    .payload(productService.getProductById(storeProductId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{id}")
    @Operation(summary = "update product by id")
    public ResponseEntity<ProductResponse<Product>> updateProductById(
            @RequestBody ProductRequest productRequest, @PathVariable("id") Integer productId
    ){
        ProductResponse<Product> response=null;
        Integer idproductupdate=productService.updateProduct(productRequest,productId);
        if(idproductupdate!=null){
            response=ProductResponse.<Product>builder()
                    .message("Uddate successfully")
                    .payload(productService.getProductById(idproductupdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }

}


