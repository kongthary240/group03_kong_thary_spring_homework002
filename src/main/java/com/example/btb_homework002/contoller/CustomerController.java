package com.example.btb_homework002.contoller;


import com.example.btb_homework002.model.entity.Customer;
import com.example.btb_homework002.model.request.CustomerRequest;
import com.example.btb_homework002.model.response.CustomerResponse;
import com.example.btb_homework002.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/all")
    @Operation( summary = "get all customers")
    public ResponseEntity <CustomerResponse <List<Customer>>> getAllCustomer(){
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .message("Fetch data successfully")
                .payload(customerService.getAllCustomer())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response) ;

    }
    @GetMapping("/{id}")
    @Operation(summary = "Get customer by id")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer customerId){
        CustomerResponse<Customer> response=null;
        if(customerService.getCustomerById(customerId)!=null){
            response =CustomerResponse.<Customer>builder()
                    .message("fetch data successfully by id")
                    .payload(customerService.getCustomerById(customerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response=CustomerResponse.<Customer>builder()
                    .message("Data not found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete customer by id")
    public ResponseEntity<CustomerResponse<String>>deleteCustomerById(@PathVariable("id") Integer customerId){
        CustomerResponse<String> response=null;
        if(customerService.deleteCustomerId(customerId)==true){
             response=CustomerResponse.<String>builder()
                    .message("Delete successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }

        return ResponseEntity.ok(response);

    }
    @PostMapping
    @Operation(summary = "Save new Customer")
    public ResponseEntity<CustomerResponse<Customer>> addnewCustomer(@RequestBody CustomerRequest customerRequest){
        Integer storeCustomerId=customerService.addNewCustomer(customerRequest);
        if(storeCustomerId!=null){
            CustomerResponse<Customer> response=CustomerResponse.<Customer> builder()
                    .message("Add successfully")
                    .payload(customerService.getCustomerById(storeCustomerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
    @PutMapping("/{id}")
    @Operation(summary = "update cutomer by id")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomerById(
            @RequestBody CustomerRequest customerRequest, @PathVariable("id") Integer customerId
    ){
        CustomerResponse<Customer> response=null;
        Integer idcustomerupdate=customerService.updateCustomer(customerRequest,customerId);
        if(idcustomerupdate!=null){
            response=CustomerResponse.<Customer>builder()
                    .message("Uddate successfully")
                    .payload(customerService.getCustomerById(idcustomerupdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }



}
