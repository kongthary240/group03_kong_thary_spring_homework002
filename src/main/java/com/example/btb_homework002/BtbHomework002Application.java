package com.example.btb_homework002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtbHomework002Application {

    public static void main(String[] args) {
        SpringApplication.run(BtbHomework002Application.class, args);
    }

}
