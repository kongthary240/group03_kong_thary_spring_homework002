package com.example.btb_homework002.service.ServiceImp;

import com.example.btb_homework002.model.entity.Customer;
import com.example.btb_homework002.model.entity.Product;
import com.example.btb_homework002.model.request.CustomerRequest;
import com.example.btb_homework002.model.request.ProductRequest;
import com.example.btb_homework002.repository.ProductRepository;
import com.example.btb_homework002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductbyID(productId);
    }

    @Override
    public boolean deleteProductId(Integer productId) {
        return productRepository.deleteProductById(productId);
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        Integer productId=productRepository.saveProduct(productRequest);
        return productId;
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer productId) {
        Integer productIdupdate=productRepository.updateproduct(productRequest,productId);

        return productIdupdate;
    }


}

