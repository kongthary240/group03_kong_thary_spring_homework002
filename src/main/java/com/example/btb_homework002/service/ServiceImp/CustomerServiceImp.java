package com.example.btb_homework002.service.ServiceImp;

import com.example.btb_homework002.model.entity.Customer;
import com.example.btb_homework002.model.request.CustomerRequest;
import com.example.btb_homework002.repository.CustomerRepository;
import com.example.btb_homework002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {

        return customerRepository.getCustomerbyID(customerId);
    }

    @Override
    public boolean deleteCustomerId(Integer customerId) {

        return customerRepository.deleteCustomerById(customerId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        Integer customerId=customerRepository.saveCustomer(customerRequest);
        return customerId;
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customerId) {
        Integer customerIdupdate=customerRepository.updatecustomer(customerRequest,customerId);
        return customerIdupdate ;
    }
}
