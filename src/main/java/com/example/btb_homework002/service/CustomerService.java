package com.example.btb_homework002.service;

import com.example.btb_homework002.model.entity.Customer;
import com.example.btb_homework002.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {


    List<Customer> getAllCustomer();
    Customer getCustomerById(Integer customerId);
    boolean deleteCustomerId(Integer customerId);
    Integer addNewCustomer(CustomerRequest customerRequest);
    Integer updateCustomer(CustomerRequest customerRequest, Integer customerId);
}
