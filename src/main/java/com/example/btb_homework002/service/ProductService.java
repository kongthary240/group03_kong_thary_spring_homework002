package com.example.btb_homework002.service;

import com.example.btb_homework002.model.entity.Customer;
import com.example.btb_homework002.model.entity.Product;
import com.example.btb_homework002.model.request.CustomerRequest;
import com.example.btb_homework002.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();
    Product getProductById(Integer productId);
    boolean deleteProductId(Integer productId);
    Integer addNewProduct(ProductRequest productRequest);
    Integer updateProduct(ProductRequest productRequest, Integer productId);

}
