

CREATE TABLE customer_tb(
    id SERIAL PRIMARY KEY ,
    name VARCHAR(240),
    address VARCHAR(200),
    phone VARCHAR(100)
);


CREATE TABLE invoice_tb(
    id SERIAL PRIMARY KEY ,
    date VARCHAR(200),
    customer_id INT  REFERENCES customer_tb(id)
);

CREATE TABLE invoice_detail_tb(
    id SERIAL PRIMARY KEY ,
    invoice_id INT REFERENCES invoice_tb(id) ON UPDATE CASCADE ON DELETE CASCADE ,
    product_id INT REFERENCES product_tb(id) ON UPDATE CASCADE ON DELETE CASCADE
--     PRIMARY KEY (invoice_id,product_id)
);

CREATE TABLE product_tb(
    id SERIAL PRIMARY KEY ,
    name VARCHAR(200),
    price VARCHAR(220)
);
